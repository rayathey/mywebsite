use stylist::css;
use yew::prelude::*;
use yew_router::prelude::*;

#[derive(Clone, Routable, PartialEq)]
enum Route {
    #[at("/")]
    Home,
    #[at("/secure")]
    Secure,
    #[not_found]
    #[at("/404")]
    NotFound,
    #[at("/about")]
    About,
}

#[function_component(Secure)]
fn secure() -> Html {
    let navigator = use_navigator().unwrap();

    let onclick = Callback::from(move |_| navigator.push(&Route::Home));
    html! {
        <div>
            <h1>{ "Secure" }</h1>
            <button {onclick}>{ "Go Home" }</button>
        </div>
    }
}

fn switch(routes: Route) -> Html {
    match routes {
        Route::Home => html! { <h1>{ "Home" }</h1> },
        Route::Secure => html! {
            <Secure />
        },
        _ => html! { <h1>{ "404" }</h1> },
    }
}

#[function_component(Main)]
fn app() -> Html {
    html! {
        <div class={css!("
        li {
            display: inline;
            float: left;
          }
          
          a {
            display: block;
            padding: 8px;
            background-color: #dddddd;
          }
          ul {
            background-color: #dddddd;
          }
          ")}>
            <div>
                <ul>
                    <li><a href={"/"}>{" Home "}</a></li>
                    <li><a href={"news"}>{" News "}</a></li>
                    <li><a href={"contact"}>{" Contact "}</a></li>
                    <li><a href={"about"}>{" About "}</a></li>
                </ul>
            </div>
            <br/>
            <BrowserRouter>
                <Switch<Route> render={switch} /> // <- must be child of <BrowserRouter>
            </BrowserRouter>
        </div>
    }
}

fn main() {
    yew::Renderer::<Main>::new().render();
}
